<p align="center"><img src="https://api.aposoftworks.com/storage/image/ehRdFIz6tqiERXID1SIXAeu0mmTBKLdixIXsNj9s.png" width="256"></p>

# Açai Stubber Module

[![Build Status](https://travis-ci.org/AcaiFramework/stubber.svg?branch=production)](https://travis-ci.org/AcaiFramework/stubber) [![Support](https://img.shields.io/badge/Patreon-Support-orange.svg?logo=Patreon)](https://www.patreon.com/rafaelcorrea)

Stubber is a simple and configurable template engine, helping you create commonly used file templates known as stubs.